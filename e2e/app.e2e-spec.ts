import { AvivChatPage } from './app.po';

describe('aviv-chat App', () => {
  let page: AvivChatPage;

  beforeEach(() => {
    page = new AvivChatPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
