// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyB3olcZ78u1hb98M38UaLs7nKDol7Dk6Lc',
    authDomain: 'agronomistance.firebaseapp.com',
    databaseURL: 'https://agronomistance.firebaseio.com',
    projectId: 'agronomistance',
    storageBucket: 'agronomistance.appspot.com',
    messagingSenderId: '923952730973'
  }
};
