export const environment = {
  production: true,
   firebase: {
    apiKey: 'AIzaSyB3olcZ78u1hb98M38UaLs7nKDol7Dk6Lc',
    authDomain: 'agronomistance.firebaseapp.com',
    databaseURL: 'https://agronomistance.firebaseio.com',
    projectId: 'agronomistance',
    storageBucket: 'agronomistance.appspot.com',
    messagingSenderId: '923952730973'
  }
};
