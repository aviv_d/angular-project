import { Injectable } from '@angular/core';
import { Room, Line } from './chat/room/room';
import { AngularFireDatabaseModule, FirebaseObjectObservable, AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ChatService {

  constructor(private db: AngularFireDatabase) { }

  getRoom(selectedId: string): FirebaseObjectObservable<any> {
    return this.db.object('/items/' + selectedId);
  }

}
