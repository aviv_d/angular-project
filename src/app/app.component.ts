import { Component } from '@angular/core';
import { AngularFireDatabaseModule, AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  items: FirebaseListObservable<any[]>;

  constructor(private db: AngularFireDatabase) {
    this.items = db.list('chat/rooms');
  }

  createNewRoom(roomName: HTMLInputElement) {
    this.items.push({
      name: roomName.value,
      lines: ['welcome to the chat ' + roomName.value]
    });
    console.log(roomName.value);
  }
}
