import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';


import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import * as firebase from 'firebase/app';

import { environment } from '../environments/environment';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RoomComponent } from './chat/room/room.component';
import { LineComponent } from './chat/line/line.component';
import { HomeComponent } from './pages/home/home.component';

import { ChatService } from './chat.service';

@NgModule({
  declarations: [
    AppComponent,
    RoomComponent,
    LineComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule
  ],
  providers: [ChatService],
  bootstrap: [AppComponent]
})
export class AppModule { }
