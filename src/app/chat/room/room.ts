export class Room {
    id: number;
    name: string;
    lines: Line[];
}

export class Line {
    text: string;
    author: string;
    timestamp: string;

    constructor(text: string, author: string, timestamp: string) {
        this.text = text;
        this.author = author;
        this.timestamp = timestamp;
    }
}
