import { Component, OnInit } from '@angular/core';
import { ChatService } from 'app/chat.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import 'rxjs/add/operator/switchMap';
import { Observable } from 'rxjs/Observable';
import { Room } from './room';
import { FirebaseObjectObservable, AngularFireDatabaseModule, AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';

@Component({
  templateUrl: './room.component.html',
  styleUrls: ['./room.component.css']
})
export class RoomComponent implements OnInit {
  room: any;
  private selectedId: string;
  _lines: string[];

  constructor(private route: ActivatedRoute,
    private router: Router,
    private db: AngularFireDatabase) { }

  ngOnInit() {
   this.route.params.subscribe(params => {
     this.selectedId = params['id'];
      this.room = this.db.object('/chat/rooms/' + params['id']);
      this.room.subscribe(params => {
        this._lines = params.lines;
      });
    });
  }
  test() {
    console.log(this.room.name);
  }
  sendLine(messageText: HTMLInputElement) {
    this._lines.push(messageText.value);
    console.log(this._lines);
    this.db.object('/chat/rooms/' + this.selectedId).update({lines: this._lines});
  }
}
